FROM golang:1.15

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64

RUN apt-get update && apt-get install -y git build-essential curl unzip netcat ruby rubygems
RUN pwd 
RUN ls -Ralh 

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
COPY go.mod .
COPY go.sum .
RUN go mod download

# Copy the code into the container
COPY . .

# Build the application
RUN make build

# Install grpcurl
RUN go get github.com/fullstorydev/grpcurl/... && go install github.com/fullstorydev/grpcurl/cmd/grpcurl

# Install grpc-health-probe
ADD https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.6/grpc_health_probe-linux-amd64 /usr/local/bin/grpc-health-probe
RUN chmod 555 /usr/local/bin/grpc-health-probe

# Export necessary port
EXPOSE 8001 8002 8080

# Command to run when starting the container
CMD ["/build/spamcheck", "config", "config/config.toml"]
#ENTRYPOINT ["/build/spamcheck", "config", "config/config.toml"]
#ENTRYPOINT /build/spamcheck config config/config.toml
#& while true ; do printf "HTTP/1.1 200 OK\n\n$(date)" | netcat -l -p 8080 ; done
