package validations

import (
	"context"
	"testing"

	"github.com/golang/protobuf/ptypes"
	"github.com/stretchr/testify/assert"
	spamcheck "gitlab.com/gitlab-org/spamcheck/api/v1"
)

func TestValidateIssue(t *testing.T) {
	user := spamcheck.User{
		Username:  "Mr. Stupendous",
		Emails:    []*spamcheck.User_Email{},
		Org:       "gitlab",
		CreatedAt: ptypes.TimestampNow(),
	}

	t.Run("testing with non-spam Issue", func(t *testing.T) {
		validations := CreateSpamCheckValidations()

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "Test title",
			Description:   "test description",
			UserInProject: true,
			CreatedAt:     ptypes.TimestampNow(),
		}

		err := validations.ValidateIssue(context.Background(), &issue)

		assert.Nil(t, err)
	})

	t.Run("testing with Issue with no title", func(t *testing.T) {
		validations := CreateSpamCheckValidations()

		issue := spamcheck.Issue{
			User:          &user,
			Description:   "test description",
			UserInProject: true,
			CreatedAt:     ptypes.TimestampNow(),
		}

		err := validations.ValidateIssue(context.Background(), &issue)

		assert.NotNil(t, err)
	})

	t.Run("testing with Issue with no description", func(t *testing.T) {
		validations := CreateSpamCheckValidations()

		issue := spamcheck.Issue{
			User:          &user,
			Title:         "test title",
			UserInProject: true,
			CreatedAt:     ptypes.TimestampNow(),
		}

		err := validations.ValidateIssue(context.Background(), &issue)

		assert.Nil(t, err)
	})
}
