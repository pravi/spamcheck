# spamcheck

## Development

Install the [g go version manager](https://github.com/stefanmaric/g) and the
correct go version:

```bash
curl -sSL https://git.io/g-install | sh -s
# Restart shell session here
g install 1.15
```

Clone this repo, then build and run the container:

```bash
git clone git@gitlab.com:gitlab-com/gl-security/engineering-and-research/automation-team/spam/spamcheck.git
cd spamcheck
cp config/config.toml.example config/config.toml
# Customize config/config.toml if necessary
make run
```

Dependencies are stored in `go.mod`.

For generating the Ruby gem that is then used by GitLab you'll need to install bundler.

```
gem install bundler
```

## Advanced Configuration

`config/config.yml` has more configuration options.

## Concept overview

## Testing

Tests are files with the `_test.go` suffix. Typically, they sit next to the
files they're testing in the directory structure.

### Test suite

Run the test suite:

```
go test ./...
```

### Manually

Run via `go run main.go config config/config.toml` or just run `make`.

The REST endpoint accepts JSON that will get translated to protobufs:

```bash
curl -H 'Content-Type: application/json' -d "$(cat examples/checkforspamissue.json)" http://localhost:8080/v1/issue_spam
```

To test the gRPC endpoint, use [grpcurl](https://github.com/fullstorydev/grpcurl):

```bash
grpcurl -plaintext -d "$(cat examples/checkforspamissue.json)" localhost:8001 spamcheck.SpamcheckService/CheckForSpamIssue
```

By default, it will return an empty object, e.g. `{}`, because Protobufs don't
encode 0-value fields and `SpamVerdict.Verdict` is an enum whose default value
is `ALLOW` which is 0. This default return value is in
`app/controllers/spamcheck.go`

There is a `/healthz` endpoint used for Kubernetes probes and uptime checks:

```bash
curl localhost:8080/healthz
grpcurl -plaintext localhost:8001 spamcheck.SpamcheckService/Healthz
```

## Publishing a new gem version

1. Make sure to update `ruby/spamcheck/version.rb` with the new version number. Keep in mind that `bundle update` uses `--major` by default, so all minor version updates should not break backward or cross-protocol compatibility. 

2. Sign up for a [RubyGems account](RubyGems.org) if you already don't have one. You should also be an owner of the [spamcheck gem](https://rubygems.org/gems/spamcheck)

3. Create a tag for spamcheck with the version number e.g. if the version number is 0.1.0, do:

    ```bash
    git tag v0.1.0
    ```

4. Check that the tag has been correctly created alongside your latest commit:

    ```bash
    git show v0.1.0
    ```

5. Push the branch with the tag:

    ```bash
    git push --tags origin <your branch>
    ```

6. Run `bundle exec ruby _support/publish-gem v0.1.0` locally. It should ask you for your rubygems email and password.

7. After a successful push, the new gem version should now be publicly available on [RubyGems.org](https://rubygems.org/gems/spamcheck) and ready to use.
